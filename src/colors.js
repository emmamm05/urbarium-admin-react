//Primary
const primary = {
	primary:	'#742E5D',
	passive:	'#8A96A0',
	gray:		'#333333',
	dark:		'#3A424C',
	mustard:	'#F4D849'
}

//Secondary
const secondary = {
	lightgray:	'#99BF24',
	blue:		'#0077FF',
	green:		'#7ED321',
	skyblue:  	'#09B7D6',
	yellow:    	'#f98c42',
	orange:    	'#f98c42',
	red:		'#f44336'
};

const neutral = {
	black:		'#000',
	white:		'#FFF',
	darkWhite:	'#eceaf1'
}

const Colors = {
	primary: primary,
	secondary: secondary,
	neutral: neutral
}



export { Colors as default, primary, secondary, neutral};
